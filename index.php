<?php

    require_once('animal.php');
    require_once('bus.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "Name: " . $sheep->nama; 
    echo "Legs: " . $sheep->kaki; 
    echo "cold blooded: " . $sheep->cold_blooded; 

    $kodok = new frog("buduk");
    echo "Name: " . $kodok->nama; 
    echo "Legs: " . $kodok->kaki; 
    echo "cold blooded: " . $kodok->cold_blooded; 
    echo $kodok->jump(); 

    $sungokong = new Ape("kera sakti");
    echo "Name: " . $sungokong->nama; 
    echo "Legs: " . $sungokong->kaki; 
    echo "cold blooded: " . $sungokong->cold_blooded; 
    echo $sungokong->yell();
?>